<?php
    if (isset($_POST['Submit'])) {

    $Nama = $_POST['Nama'];
    $Mapel = $_POST['Mapel'];
    $UTS = $_POST['UTS'];
    $UAS = $_POST['UAS'];
    $Tugas = $_POST['Tugas'];

    $nilai_UTS = $UTS * 0.35;
    $nilai_UAS = $UAS * 0.5;
    $nilai_Tugas = $Tugas * 0.15;

    $nilai_total = $nilai_UTS + $nilai_UAS + $nilai_Tugas;

    if ($nilai_total >= 90 && $nilai_total <= 100) {
        $grade = "A";
    } elseif ($nilai_total > 70 && $nilai_total < 90) {
        $grade = "B";
    } elseif ($nilai_total > 50 && $nilai_total <= 70) {
        $grade = "C";
    } elseif ($nilai_total <= 50) {
        $grade = "D";
    }
}
?>

<!doctype html>
<html lang="en">
  
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Form Penilaian Siswa</title>
</head>

<body>
    <h1><center>FORM NILAI SISWA</center></h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 border border-primary mt-2 p-3">
                <form method ="POST">
                    <div class="mb-3">
                        <label for="exampleInputNama" class="form-label">Nama Lengkap Siswa</label>
                        <input type="text" name="Nama" required="" class="form-control" id="exampleInputName">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputMapel" class="form-label">Nama Mapel</label>
                        <input type="text" name="Mapel" required="" class="form-control" id="exampleInputMapel">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputTugas" class="form-label">Nilai Tugas</label>
                        <input type="number" min="10" max="100" name="Tugas" required="" class="form-control" id="exampleInputTugas">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputUTS" class="form-label">Nilai UTS</label>
                        <input type="number" min="10" max="100" name="UTS" required="" class="form-control" id="exampleInputUTS">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputUAS" class="form-label">Nilai UAS</label>
                        <input type="number" min="10" max="100" name="UAS" required="" class="form-control" id="exampleInputUAS">
                    </div>
                    <button type="Kirim" name="Submit" class="btn btn-primary">Kirim</button>
                </form>
            </div>
        </div>
        <?php if(isset($_POST['Submit'])): ?>
        <div class="row justify-content-center">
            <div class="col-5 border border-primary mt-2 p-3">
                <div class="alert alert-success">
                    Nama Lengkap    : <?php echo $Nama ?> <br>
                    Mata Pelajaran  : <?php echo $Mapel ?> <br>
                    Nilai Tugas     : <?php echo $Tugas ?> <br>
                    Nilai UTS       : <?php echo $UTS ?> <br>
                    Nilai UAS       : <?php echo $UAS ?> <br>
                    Nilai Total     : <?php echo $nilai_total ?> <br>
                    Grade           : <?php echo $grade ?> <br>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>
</html>